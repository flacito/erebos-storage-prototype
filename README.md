# erebos-storage-prototype

Simple JS project to store and retrieve compressed, encrypted, user data in Eth Swarm.

## Prerequisites

1. NodeJS

## Run it

1. Get the repo on your machine
2. `yarn install`  (or remove the yarn.lock file and `npm install`)
3. `./index.js`
