#!/usr/bin/env node

const CryptoJS = require('crypto-js');
const SwarmClient = require('@erebos/swarm-node').SwarmClient
const pako = require('pako');
const btoa = require('btoa')
const atob = require('atob')
const userdata = require('./userdata.json')

const client = new SwarmClient({
  http: 'https://swarm-gateways.net'
})

// const guid = chance.guid();

const passphrase = '93D826A6-2094-4677-981D-9D2760DE9520'

async function main() {

  let jsonStr = JSON.stringify(userdata);
  let compressedJsonArray = pako.gzip(jsonStr, { to: 'string' });
  let payload = btoa(String.fromCharCode.apply(null, compressedJsonArray))
  const encrypted = CryptoJS.AES.encrypt(payload, passphrase).toString();
  console.log(`upload size: ${encrypted.length}`)

  const hash = await client.bzz.uploadFile(encrypted, {
    contentType: 'text/plain'
  })
  console.log(`Upload complete. storage hash: ${hash}`)

  const res = await client.bzz.download(hash)
  const text = await res.text()
  console.log(`download size: ${text.length}`)

  let decryptedPayload = CryptoJS.AES.decrypt(text, passphrase)
  payload = decryptedPayload.toString(CryptoJS.enc.Utf8)
  compressedJsonArray = atob(payload).split('').map(function (c) { return c.charCodeAt(0); })
  jsonStr = pako.ungzip(compressedJsonArray, { to: 'string' });
  const f = JSON.parse(jsonStr)
  console.log(f)
}

main();